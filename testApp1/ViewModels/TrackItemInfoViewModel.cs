﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testApp1.Tools;
using testApp1.Tools.Interfaces;
using Windows.UI.Xaml.Media;

namespace testApp1.ViewModels
{
    public class TrackItemInfoViewModel : ViewModelBase, IHandle<MediaElementState>
    {

        public TrackItemInfoViewModel(INavigationManager navigationManager) : base(navigationManager)
        {
            SoundPlayer.Instance.Initialize();
            //подписываемся на изменение состояния медиа плеера
            EventAggregationProvider.EventAggregator.Subscribe(this);
        }
        private MediaElementState _mediaElementState;
        public TrackListItemViewModel Parameter { get; set; }
        protected override void OnActivate()
        {
            if (Parameter != null)
            {
                var intReleaseDate = Convert.ToInt32(Parameter.ReleaseDate);
                var CountDay = intReleaseDate - GetUnixTime();
                if (CountDay < 0)
                {
                    PlayButtonsVisibile = true;
                }
                else
                {
                    CounterReleaseDayVisibile = true;
                    ReleaseDayCount = CountDay / 86400;
                }

                TrackTitle = Parameter.TrackTitle;
                TrackDescription = Parameter.TrackDescription;
                TrackAuthor = Parameter.TrackAuthor;
                TrackImage = Parameter.TrackImage;
                TrackMp3 = Parameter.TrackMp3;
            }
        }
        //Метод получения текущей даты в unix формате
        private int GetUnixTime()
        {
            return (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public async void btnPlayPauseTrack()
        {
            switch (_mediaElementState)
            {
                case MediaElementState.Paused:
                    {
                        await SoundPlayer.Instance.Play(TrackMp3);
                    }
                    break;
                case MediaElementState.Playing:
                    {
                        await SoundPlayer.Instance.Pause();
                    }
                    break;
                case MediaElementState.Closed:
                    {
                        await SoundPlayer.Instance.Play(TrackMp3);
                    }
                    break;
                case MediaElementState.Stopped:
                    {
                        await SoundPlayer.Instance.Play(TrackMp3);
                    }
                    break;
            }
            
        }
        public async void btnPauseTrack()
        {
            await SoundPlayer.Instance.Pause();
        }
        public async void btnStopTrack()
        {
            await SoundPlayer.Instance.Stop();
        }
        //метод срабатывает, когда происходит смена состояния медиа плеера, при создании события в SoundPlayer.cs
        public void Handle(MediaElementState mediaElementState)
        {
            _mediaElementState = mediaElementState;
            switch (mediaElementState)
            {
                case MediaElementState.Stopped:
                    {
                        PlayPauseBtnContent = "\uE102";
                    }
                    break;
                case MediaElementState.Playing:
                    {
                        PlayPauseBtnContent = "\uE103";
                    }
                    break;
                case MediaElementState.Paused:
                    {
                        PlayPauseBtnContent = "\uE102";
                    }
                    break;
                case MediaElementState.Closed:
                    {
                        PlayPauseBtnContent = "\uE102";
                    }
                    break;
            }
        }

        //метод симуляции скачивания трека
        public async void btnDownloadTrack()
        {
            progressTrackDwVisibile = true;
            for (int x = 0; x <= 1000; x++)
            {
                if (x % 100 == 0)
                {
                    await Task.Delay(TimeSpan.FromSeconds(1));
                }
                progressTrackDownload++;
            }
            progressTrackDwVisibile = false;
            progressTrackDownload = 0;
        }
        private int _releaseDayCount;
        public int ReleaseDayCount
        {
            get
            {
                return _releaseDayCount;
            }
            set
            {
                this.Set(ref _releaseDayCount, value);
            }
        }

        private int _progressTrackDownload;
        public int progressTrackDownload
        {
            get
            {
                return _progressTrackDownload;
            }
            set
            {
                this.Set(ref _progressTrackDownload, value);
            }
        }
        private bool _progressTrackDwVisibile;
        public bool progressTrackDwVisibile
        {
            get
            {
                return _progressTrackDwVisibile;
            }
            set
            {
                this.Set(ref _progressTrackDwVisibile, value);
            }
        }
        private bool _playButtonsVisibile;
        public bool PlayButtonsVisibile
        {
            get
            {
                return _playButtonsVisibile;
            }
            set
            {
                this.Set(ref _playButtonsVisibile, value);
            }
        }
        private bool _counterReleaseDayVisibile;
        public bool CounterReleaseDayVisibile
        {
            get
            {
                return _counterReleaseDayVisibile;
            }
            set
            {
                this.Set(ref _counterReleaseDayVisibile, value);
            }
        }
        private string _trackTitle;
        public string TrackTitle
        {
            get
            {
                return _trackTitle;
            }
            set
            {
                this.Set(ref _trackTitle, value);
            }
        }
        private string _trackDescription;
        public string TrackDescription
        {
            get
            {
                return _trackDescription;
            }
            set
            {
                this.Set(ref _trackDescription, value);
            }
        }
        private string _trackAuthor;
        public string TrackAuthor
        {
            get
            {
                return _trackAuthor;
            }
            set
            {
                this.Set(ref _trackAuthor, value);
            }
        }
        private string _trackMp3;
        public string TrackMp3
        {
            get
            {
                return _trackMp3;
            }
            set
            {
                this.Set(ref _trackMp3, value);
            }
        }
        private ImageSource _trackImage;
        public ImageSource TrackImage
        {
            get
            {
                return _trackImage;
            }
            set
            {
                this.Set(ref _trackImage, value);
            }
        }
        private string _trackReleaseDate;
        public string TrackReleaseDate
        {
            get
            {
                return _trackReleaseDate;
            }
            set
            {
                this.Set(ref _trackReleaseDate, value);
            }
        }
        private string _playPauseBtnContent= "\uE102";
        public string PlayPauseBtnContent
        {
            get
            {
                return _playPauseBtnContent;
            }
            set
            {
                this.Set(ref _playPauseBtnContent, value);
            }
        }
    }
}
