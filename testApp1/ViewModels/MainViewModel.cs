﻿using Caliburn.Micro;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testApp1.Tools;
using testApp1.Tools.Interfaces;
using Windows.Storage;

namespace testApp1.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel(INavigationManager navigationManager) : base(navigationManager)
        {
            TrackListItems = new BindableCollection<TrackListItemViewModel>();
        }
        //читаем json файл как текст
        public async Task<string> ReadJsonFileAsync(string JsonDataPath)
        {
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri(JsonDataPath));
            using (StreamReader sRead = new StreamReader(await file.OpenStreamForReadAsync()))
            return await sRead.ReadToEndAsync();
        }
        protected override async void OnActivate()
        {
            if (TrackListItems.Count() == 0)
            {
                var jsontext = await ReadJsonFileAsync("ms-appx:///Data/data.txt");
                TrackListItemJson JsonTrackListItem = new TrackListItemJson();
                try
                {
                    //десериализация прочитанного json файла
                    JsonTrackListItem = JsonConvert.DeserializeObject<TrackListItemJson>(jsontext);
                    foreach (var item in JsonTrackListItem.tracks)
                    {
                        TrackListItems.Add(new TrackListItemViewModel(Convert.ToInt32(item.id), item.title, item.description, item.author, item.pathmp3, item.pathimg, item.releasedate));
                    }
                }
                catch (Exception ex)
                {
                    string exM = ex.Message;
                }               
            }
        }
        public void TrackListItemSelected(TrackListItemViewModel trackItem)
        {
            NavigationManager.NavigateTo<TrackItemInfoViewModel>(trackItem);
        }
        public void btnSecondPage()
        {
            NavigationManager.NavigateTo<SecondPageViewModel>();
        }
        public BindableCollection<TrackListItemViewModel> TrackListItems
        {
            get;
            private set;
        }
    }
}
