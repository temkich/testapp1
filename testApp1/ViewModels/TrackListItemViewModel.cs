﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace testApp1.ViewModels
{
    public class TrackListItemViewModel
    {
        public TrackListItemViewModel(int trackId, string trackTitle, string trackDescription, string trackAuthor,string trackMp3, string trackImage, string releaseDate)
        {
            TrackId = trackId;
            TrackTitle = trackTitle;
            TrackDescription = trackDescription;
            TrackAuthor = trackAuthor;
            TrackMp3 = trackMp3;
            TrackImage = new BitmapImage(new Uri(trackImage));
            ReleaseDate = releaseDate;
        }

        public int TrackId
        {
            get;
            private set;
        }
        public string TrackTitle
        {
            get;
            private set;
        }
        public string TrackDescription
        {
            get;
            private set;
        }
        public string TrackAuthor
        {
            get;
            private set;
        }
        public string TrackMp3
        {
            get;
            private set;
        }
        public ImageSource TrackImage
        {
            get;
            private set;
        }
        public string ReleaseDate
        {
            get;
            private set;
        }

    }
}
