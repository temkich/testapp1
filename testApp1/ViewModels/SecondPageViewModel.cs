﻿using Caliburn.Micro;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testApp1.Tools;
using testApp1.Tools.Interfaces;
using Windows.Storage;
using Windows.UI.Xaml;

namespace testApp1.ViewModels
{
    public class SecondPageViewModel : ViewModelBase
    {
        //таймер для смены картинки, картинка меняется каждые 5 секунд
        DispatcherTimer _dispatcherTimer;
        public SecondPageViewModel(INavigationManager navigationManager) : base(navigationManager)
        {
            TrackListItems = new BindableCollection<TrackListItemViewModel>();

            _dispatcherTimer = new DispatcherTimer();
            _dispatcherTimer.Tick += _dispatcherTimer_Tick;
            _dispatcherTimer.Interval = TimeSpan.FromSeconds(5);
        }


        //Читаем json файл как текст
        public async Task<string> ReadJsonFileAsync(string JsonDataPath)
        {
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri(JsonDataPath));
            using (StreamReader sRead = new StreamReader(await file.OpenStreamForReadAsync()))
                return await sRead.ReadToEndAsync();
        }
        protected override async void OnActivate()
        {
            if (TrackListItems.Count() == 0)
            {
                var jsontext = await ReadJsonFileAsync("ms-appx:///Data/data.txt");
                TrackListItemJson JsonTrackListItem = new TrackListItemJson();
                try
                {
                    JsonTrackListItem = JsonConvert.DeserializeObject<TrackListItemJson>(jsontext);
                    foreach (var item in JsonTrackListItem.tracks)
                    {
                        TrackListItems.Add(new TrackListItemViewModel(Convert.ToInt32(item.id), item.title, item.description, item.author, item.pathmp3, item.pathimg,item.releasedate));
                    }
                }
                catch (Exception ex)
                {
                    string exM = ex.Message;
                }

                var rnd = new Random();
                SelectedPivotIndex = rnd.Next(0, TrackListItems.Count()-1);
                _dispatcherTimer.Start();
            }
        }
        private void _dispatcherTimer_Tick(object sender, object e)
        {
            var tempSelectedPivot = SelectedPivotIndex;
            tempSelectedPivot++;
            if (tempSelectedPivot > (TrackListItems.Count() - 1))
            {
                tempSelectedPivot = 0;
            }
            SelectedPivotIndex = tempSelectedPivot;

        }
        public void TrackListItemSelected(TrackListItemViewModel trackItem)
        {
            NavigationManager.NavigateTo<TrackItemInfoViewModel>(trackItem);
        }
        public void TappedClick(TrackListItemViewModel trackItem)
        {
            NavigationManager.NavigateTo<TrackItemInfoViewModel>(trackItem);
        }
        public BindableCollection<TrackListItemViewModel> TrackListItems
        {
            get;
            private set;
        }
        private int _selectedPivotIndex;
        public int SelectedPivotIndex
        {
            get
            {
                return _selectedPivotIndex;
            }
            set
            {
                this.Set(ref _selectedPivotIndex, value);
            }
        }
    }
}
