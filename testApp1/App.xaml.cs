﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using testApp1.Tools;
using testApp1.Tools.Interfaces;
using testApp1.ViewModels;
using testApp1.Views;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace testApp1
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App
    {
        private WinRTContainer container;

        public App()
        {
            InitializeComponent();
        }
       
        protected override void Configure()
        {

            MessageBinder.SpecialValues.Add("$clickeditem", c => ((ItemClickEventArgs)c.EventArgs).ClickedItem);
            //MessageBinder.SpecialValues.Add("$eventArgs", c => ((CancelEventArgs)c.EventArgs));

            container = new WinRTContainer();
            container.RegisterWinRTServices();

            // Register services
            //container.PerRequest<IMediaPlayer, MediaPlayer>();

            //_container.Singleton<IEventAggregator, EventAggregator>();
            container.Singleton<IEventAggregator, EventAggregator>();
            // Register view models
            container.PerRequest<MainViewModel>();
            container.PerRequest<TrackItemInfoViewModel>();
            container.PerRequest<TrackListItemViewModel>();
            container.PerRequest<SecondPageViewModel>();



        }

        protected override void PrepareViewFirst(Frame rootFrame)
        {

            //rootFrame.Style = this.Resources["RootFrameStyle"] as Style;
            // Register the CM navigation service
            container.RegisterNavigationService(rootFrame);

            // Register the navigation manager
            var navigationManager = new NavigationManager(container.GetInstance<INavigationService>());
            container.RegisterInstance(typeof(INavigationManager), null, navigationManager); ;
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            if (args.PreviousExecutionState == ApplicationExecutionState.Running)
                return;

            DisplayRootView<MainView>();

        }

        protected override object GetInstance(Type service, string key)
        {
            return container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            container.BuildUp(instance);
        }

        //protected override IEnumerable<Assembly> SelectAssemblies()
        //{
        //    List<Assembly> assemblies = base.SelectAssemblies().ToList();
        //    assemblies.Add(typeof(MainViewModel).GetTypeInfo().Assembly);

        //    return assemblies;
        //}
    }
}