﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testApp1.Tools.Interfaces
{
    public interface INavigationManager
    {
        bool CanGoBack { get; }

        void GoBack();

        void NavigateTo<TViewModel>(object parameter = null) where TViewModel : ViewModelBase;
    }
}
