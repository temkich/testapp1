﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testApp1.Tools.Interfaces;

namespace testApp1.Tools
{
    public class ViewModelBase : Screen
    {
        private readonly INavigationManager navigationManager;

        public ViewModelBase(INavigationManager navigationManager)
        {
            this.navigationManager = navigationManager;
        }
        public ViewModelBase()
        {

        }

        public INavigationManager NavigationManager
        {
            get { return navigationManager; }
        }
    }
}
