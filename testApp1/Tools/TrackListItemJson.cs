﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testApp1.Tools
{
    public class Track
    {
        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string author { get; set; }
        public string pathmp3 { get; set; }
        public string pathimg { get; set; }
        public string releasedate { get; set; }
    }

    public class TrackListItemJson
    {
        public List<Track> tracks { get; set; }
    }
}
