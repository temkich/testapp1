﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testApp1.Tools
{
    public class EventAggregationProvider
    {
        //стандартный класс в Caliburn.Micro для работы с собственными событиями
        static EventAggregator _eventAggregator = null;
        public static EventAggregator EventAggregator
        {
            get
            {
                if (_eventAggregator == null)
                    _eventAggregator = new EventAggregator();

                return _eventAggregator;
            }
        }
    }
}
