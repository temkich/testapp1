﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testApp1.Tools.Interfaces;

namespace testApp1.Tools
{
    public class NavigationManager : INavigationManager
    {
        private readonly INavigationService navigationService;

        public NavigationManager(INavigationService navigationService)
        {
            this.navigationService = navigationService;
        }

        public bool CanGoBack
        {
            get { return navigationService.CanGoBack; }
        }

        public void GoBack()
        {
            navigationService.GoBack();
        }

        public void NavigateTo<TViewModel>(object parameter = null) where TViewModel : ViewModelBase
        {
            navigationService.NavigateToViewModel<TViewModel>(parameter);
        }
    }
}
