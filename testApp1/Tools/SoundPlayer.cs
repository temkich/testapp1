﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testApp1.Views;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace testApp1.Tools
{
    internal class SoundPlayer : BindableBase
    {
        private static SoundPlayer instance = new SoundPlayer();
        private static MediaElement MyMediaPlayer { get; set; }

        public static SoundPlayer Instance
        {
            get
            {
                return instance;
            }
        }

        public void Initialize()
        {
            //Подключаем MediaElement к MyMediaPlayer, код которого находится в самом начале в xaml файле TrackItemInfoView.Xaml
            try
            {
                var frame = (Frame)Window.Current.Content as Frame;
                var myView = (TrackItemInfoView)frame.Content as TrackItemInfoView;

                var Grid = (Grid)myView.Content as Grid;
                var myMediaPlayer = (MediaElement)VisualTreeHelper.GetChild(Grid, 0) as MediaElement;

                SoundPlayer.MyMediaPlayer = myMediaPlayer;
                SoundPlayer.MyMediaPlayer.CurrentStateChanged += MyMediaPlayer_CurrentStateChanged;
            }
            catch (Exception dd)
            {
                var wdw = dd.Message;
                SoundPlayer.MyMediaPlayer = null;
            }
        }

        private void MyMediaPlayer_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            //При изменении состояния медиа плеера создаем событие с помощью EventAggregator
            EventAggregationProvider.EventAggregator.PublishOnUIThread(SoundPlayer.MyMediaPlayer.CurrentState);
        }

        public async Task Pause()
        {
            var mediaElement = MyMediaPlayer;

            if (mediaElement == null)
            {
                return;
            }
            await mediaElement.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                mediaElement.Pause();
            });
        }
        public async Task Stop()
        {
            var mediaElement = MyMediaPlayer;

            if (mediaElement == null)
            {
                return;
            }
            await mediaElement.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                mediaElement.Stop();
            });
        }
        public async Task Play(string trackMp3)
        {
            var mediaElement = MyMediaPlayer;

            if (mediaElement == null)
            {
                return;
            }
            if (mediaElement.CurrentState != MediaElementState.Paused)
            {
                mediaElement.Source = new Uri(trackMp3);
            }
            await mediaElement.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                    mediaElement.Play();
            });
        }
    }
}
